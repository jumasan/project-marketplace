import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    connected: false,
    user: null
  },
  mutations: {
    connect (state) {
      state.connected = true
    },
    disconnect (state) {
      state.connected = false
      state.user = null
      router.push('/')
    },
    setUser (state, user) {
      state.user = user
    },

    initialiseStore (state) {
      if (sessionStorage.getItem('store')) {
        // Replace the state object with the stored item
        this.replaceState(
          Object.assign(state, JSON.parse(sessionStorage.getItem('store')))
        )
      }
    }
  },
  actions: {}
})
store.subscribe((mutation, state) => {
  // Store the state object as a JSON string
  sessionStorage.setItem('store', JSON.stringify(state))
})

export default store
