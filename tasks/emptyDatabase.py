import django
from django.db import connection
import sys
import os


def django_setup():
    sys.path.append(os.path.join(os.curdir, 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")
    django.setup()


django_setup()


def emptyDB():
    cursor = connection.cursor()
    cursor.execute("TRUNCATE TABLE marketplace_user CASCADE;")
    cursor.execute("ALTER SEQUENCE marketplace_user_id_seq RESTART WITH 1")
    cursor.execute("ALTER SEQUENCE marketplace_atelier_id_seq RESTART WITH 1")
    cursor.execute("ALTER SEQUENCE marketplace_session_id_seq RESTART WITH 1")
    cursor.execute(
        "ALTER SEQUENCE marketplace_sessionelement_id_seq RESTART WITH 1")
    cursor.execute(
        "ALTER SEQUENCE marketplace_reservation_id_seq RESTART WITH 1")
    cursor.execute("TRUNCATE TABLE marketplace_category CASCADE;")
    cursor.execute(
        "ALTER SEQUENCE marketplace_category_id_seq RESTART WITH 1;")
    cursor.execute(
        "ALTER SEQUENCE marketplace_subcategory_id_seq RESTART WITH 1;")
    cursor.execute(
        "ALTER SEQUENCE marketplace_ateliersubcategory_id_seq RESTART WITH 1;")
    print("Terminé ! La BDD est vide")
