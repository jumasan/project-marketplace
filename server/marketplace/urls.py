from django.urls import include, path
from rest_framework import routers
from marketplace.views import *

router = routers.DefaultRouter()

# add routers
router.register('users', UserViewSet)
router.register('artistans', ArtistanViewSet)
router.register('ateliers', AtelierViewSet)
router.register('comments', CommentViewSet)
router.register('sessions', SessionViewSet)
router.register('categorys', CategoryViewSet)
router.register('subcats', SubCategoryViewSet)
router.register('ateliersubcats', AtelierSubCategoryViewSet)
router.register('sessionselemts', SessionElementViewSet)
router.register('reservations', ReservationViewSet)


urlpatterns = [
    path('', include(router.urls)),
]


