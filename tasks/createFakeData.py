# -*- coding: utf-8 -*-
from django.db import connection
import django
import sys
import os
import datetime
import random
from lorem.text import TextLorem
from faker import Faker
fake = Faker('fr_Fr')


def django_setup():

    sys.path.append(os.path.join(os.curdir, 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")
    django.setup()


django_setup()


def getDateFromAnotherDate(date, days_after_today=0):
    return date + datetime.timedelta(days=days_after_today)


def removeSpaces(string_with_space):
    parts_of_string = string_with_space.split(' ')
    return ''.join(parts_of_string)


def createFakerUserData():
    from marketplace.models import User
    fullname = fake.name()
    mail = random.choice(["@gmail.com", "@protonmail.com"])
    user_firstname = fullname.split(' ')[0]
    user_lastname = fullname.split(' ')[1]

    def passwordGeneration(length):
        if not isinstance(length, int) or length < 8:
            raise ValueError("temp password must have positive length")

        chars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"
        return ''.join(random.choice(chars) for _ in range(length))

    # Création du user dans la BDD
    user = User.objects.create(
        user_firstname=user_firstname,
        user_lastname=user_lastname,
        user_email=(random.choice(user_firstname) +
                    str(random.randint(0, 9)) + user_lastname+mail).lower(),
        user_password=passwordGeneration(10),
        user_birthdate=fake.profile()["birthdate"],
        user_created_on=fake.date_time_between(
            start_date="-30y", end_date="now", tzinfo=None).date(),
        user_last_login=datetime.datetime.now().date(),
        user_is_active=random.choice([True, False]),
        user_cguv=random.choice([True, False]),
        user_newsletter=random.choice([True, False])
    )
    return user


def createFakerArtistan(user_id):
    from marketplace.models import Artistan

    def create_ape():
        ape = [fake.random.randint(1, 10) for _ in range(3)]
        ape.append(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ"))
        strApe = ''.join(map(str, ape))
        return(strApe)

    artistan = Artistan.objects.create(
        user_id=user_id,
        artistan_address=fake.address(),
        artistan_zipcode=removeSpaces(fake.postcode()),
        artistan_city=fake.city(),
        artistan_siren=removeSpaces(fake.siren()),
        artistan_siret=removeSpaces(fake.siret()),
        artistan_ape=create_ape()

    )
    return artistan


def createFakerAtelier(user_id):
    from marketplace.models import Atelier
    lorem = TextLorem(wsep=' ', srange=(2, 6))
    nb_days = random.randint(1, 3)
    nb_hours = nb_days * 7
    atelier = Atelier.objects.create(
        user_id=user_id,
        atelier_address=fake.address(),
        atelier_zipcode=removeSpaces(fake.postcode()),
        atelier_city=fake.city(),
        atelier_environment=random.choice(["int", "ext"]),
        atelier_public=random.choice(["adlt", "enf", "hand"]),
        atelier_price=random.randint(25, 300),
        atelier_nb_days=nb_days,
        atelier_nb_hours=nb_hours,
        atelier_title=lorem.sentence(),
        atelier_description=fake.text(max_nb_chars=200, ext_word_list=None),
        atelier_level=random.choice(['D', 'I', 'C', 'T']),
        atelier_language=random.choice(['fr', 'esp', 'port', 'ger', 'eng']),
        atelier_informations_pratiques=fake.text(
            max_nb_chars=100, ext_word_list=None),
        atelier_is_active=random.choice([True, False]),
        atelier_is_validated=random.choice([True, False])
    )
    return atelier


def createFakerSession(atelier_id):
    from marketplace.models import Session
    session = Session.objects.create(
        session_nb_participants=random.randint(5, 20),
        atelier_id=atelier_id
    )
    return session


def createFakerSession_element(session_id, nb_hours, previous_element_date=None):
    from marketplace.models import SessionElement
    if previous_element_date:
        element_date = getDateFromAnotherDate(
            previous_element_date, random.randint(1, 2))
    else:
        element_date = fake.date_time_between(start_date="-1y", tzinfo=None)

    start_hour = 8
    session_element = SessionElement.objects.create(
        element_date=element_date,
        element_start_hour=datetime.datetime(
            element_date.year, element_date.month, element_date.day, start_hour, 0),
        element_end_hour=datetime.datetime(
            element_date.year, element_date.month, element_date.day, start_hour + nb_hours, 0),
        session_id=session_id
    )
    return session_element


def createFakerComment(user_id, atelier_id):
    from marketplace.models import Comment
    lorem = TextLorem(wsep=' ', srange=(2, 10))

    comment = Comment.objects.create(
        comment_content=lorem.sentence(),
        comment_datetime=fake.date_time_between(
            start_date="-1y", end_date="now", tzinfo=None).date(),
        comment_is_validated=random.choice([True, False]),
        atelier_id=atelier_id,
        user_id=user_id
    )
    return comment


def createFakerReservation(user_id, session):
    from marketplace.models import Reservation, SessionElement
    # Règle arbitraire pour créer des reservations "deleted"
    reservation_status = 'deleted' if user_id % 3 == 0 and session.id % 2 == 0 else 'online'

    # Récupérer la date du premier jour de la session car la réservation doit être faite avant
    first_day_session = SessionElement.objects.filter(
        session_id=session).order_by('element_date').first().element_date

    reservation = Reservation.objects.create(
        reservation_status=reservation_status,
        reservation_date=getDateFromAnotherDate(
            first_day_session, random.randint(-20, 0)),
        session_id=session.id,
        user_id=user_id
    )


def createCategoriesAndSubcategories():
    cursor = connection.cursor()
    cursor.execute("TRUNCATE TABLE marketplace_category CASCADE;")
    cursor.execute(
        "ALTER SEQUENCE marketplace_category_id_seq RESTART WITH 1;")
    cursor.execute(
        "ALTER SEQUENCE marketplace_subcategory_id_seq RESTART WITH 1;")

    from marketplace.models import Category, SubCategory
    categories_list = [{"category": "Artisanat d'art", "sub_categories": ["Bijouterie", "Bois", "Ameublement",
                                                                          "Metal", "Verre", "Terre", "Cuir", "Textile", "Pierre"]},
                       {"category": "Art et cultures", "sub_categories": [
                           "Arts plastiques", "Arts visuels / Média", "Loisirs créatifs", "Design mode", "Street art"]},
                       {"category": "Arts et spectacles", "sub_categories": ["Danse", "Musique", "Théâtre"]}]

    sub_categories_list = []
    for cat in categories_list:
        category = Category.objects.create(category_name=cat["category"])
        for subcat in cat["sub_categories"]:
            sub_category = SubCategory.objects.create(
                category=category,
                sub_category_name=subcat
            )
            sub_categories_list.append(sub_category)
    return sub_categories_list


def createSubcategoriesAteliers(list_ateliers, list_sub_categories):
    from marketplace.models import AtelierSubCategory
    for atelier in list_ateliers:
        subcategory = random.choice(list_sub_categories)
        AtelierSubCategory.objects.create(
            sub_category=subcategory,
            atelier=atelier
        )


def createFakeData():
    list_users = []
    list_artistan = []
    list_ateliers = []
    list_sessions = []

    # Crée 10 users
    for i in range(10):
        user = createFakerUserData()
        list_users.append(user)

        # 5 users sont des artistans
        if i < 5:
            artistan = createFakerArtistan(user.id)
            list_artistan.append(artistan)

            # Crée entre 1 et 3 ateliers par artistan
            for _ in range(random.randint(1, 3)):
                atelier = createFakerAtelier(artistan.user_id)
                list_ateliers.append(atelier)
                atelier_nb_hours = atelier.atelier_nb_hours

                # Crée de 0 à 3 sessions par atelier
                for b in range(random.randint(0, 3)):
                    session = createFakerSession(atelier.id)
                    list_sessions.append(session)

                    # Crée une sessionelement par jour d'atelier
                    for nb_days in range(atelier.atelier_nb_days):
                        if nb_days == 0:
                            session_element = createFakerSession_element(
                                session.id, 7)
                        else:
                            session_element = createFakerSession_element(
                                session.id, 7, session_element.element_date)

    # Création de 0 à 2 commentaires par user
    for user in list_users:
        for _ in range(random.randint(0, 3)):
            createFakerComment(user.id, random.choice(list_ateliers).id)

    # Création de 0 à 4 réservations par user
    for user in list_users:
        for _ in range(random.randint(0, 4)):
            createFakerReservation(user.id, random.choice(list_sessions))

    # Création des catégories et des sous-catégories liées
    list_sub_categories = createCategoriesAndSubcategories()

    # Liaison des ateliers à des sous-catégories
    createSubcategoriesAteliers(list_ateliers, list_sub_categories)

    print("Terminé ! La BDD a été remplie")
