from django.shortcuts import render
from django.contrib.auth.models import *
from rest_framework import filters

from rest_framework.viewsets import ModelViewSet
from marketplace.models import *
from marketplace.serializers import *

# Create your views here.


class UserViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('=user_email',)


class ArtistanViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Artistan.objects.all()
    serializer_class = ArtistanSerializer


class AtelierViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Atelier.objects.all()
    serializer_class = AtelierSerializer


class CommentViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class SessionViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Session.objects.all()
    serializer_class = SessionSerializer


class CategoryViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class SubCategoryViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer


class AtelierSubCategoryViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = AtelierSubCategory.objects.all()
    serializer_class = AtelierSubCategorySerializer


class SessionElementViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = SessionElement.objects.all()
    serializer_class = SessionElementSerializer


class ReservationViewSet(ModelViewSet):
    '''
    This comment will be visible in the web interface
    '''
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
