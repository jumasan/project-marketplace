import Vue from 'vue'
import Router from 'vue-router'
import Accueil from './views/Accueil.vue'
import Ateliers from './views/Ateliers.vue'
import AtelierDetail from './views/AtelierDetail.vue'
import CreateCategory from './views/AtelierCreate/CreateCategory'
import CreateLieu from './views/AtelierCreate/CreateLieu'
import CreatePhoto from './views/AtelierCreate/CreatePhoto'
import CreateDuree from './views/AtelierCreate/CreateDuree'
import CreatePrix from './views/AtelierCreate/CreatePrix'
import CreateDescription from './views/AtelierCreate/CreateDescription'
import CreateSession from './views/CreateSession'
import ProDashboard from './views/pro/ProDashboard'
import ProProfil from './views/pro/ProProfil'
import ProAteliers from './views/pro/ProAteliers'
import ProSessions from './views/pro/ProSessions'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'accueil',
      component: Accueil
    },
    {
      path: '/ateliers',
      name: 'ateliers',
      component: Ateliers
    },
    {
      path: '/ateliers/:id',
      name: 'atelierdetail',
      component: AtelierDetail
    },
    {
      path: '/ateliers/create/category',
      name: 'createcategory',
      component: CreateCategory
    },
    {
      path: '/ateliers/create/lieu',
      name: 'createlieu',
      component: CreateLieu
    },
    {
      path: '/ateliers/create/photo',
      name: 'createphoto',
      component: CreatePhoto
    },
    {
      path: '/ateliers/create/duree',
      name: 'createduree',
      component: CreateDuree
    },
    {
      path: '/ateliers/create/prix',
      name: 'createprix',
      component: CreatePrix
    },
    {
      path: '/ateliers/create/description',
      name: 'createcdescription',
      component: CreateDescription
    },
    {
      path: '/ateliers/createsession',
      name: 'createsession',
      component: CreateSession
    },
    {
      path: '/prodashboard',
      name: 'prodashboard',
      component: ProDashboard
    },
    {
      path: '/proprofil',
      name: 'proprofil',
      component: ProProfil
    },
    {
      path: '/proateliers',
      name: 'proateliers',
      component: ProAteliers
    },
    {
      path: '/proateliers/:id/sessions',
      name: 'prosessions',
      component: ProSessions
    },
  ]
})
