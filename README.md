# marketplace

- Ce projet consiste à créer un site e-commerce, c-à-d une marketplace de mise en relation entre prestataires de service et clients finaux.
- Une page d'offres ou "produits" est prévue, sous forme de vignettes et un filtre sur 5 critères
- Les tec utilisées sont en Back --> Django Rest Framework ; en Front --> Vue.js ; Base de données : Postgres


### Objectifs du module Fil Rouge dans le cadre de la formation Fullstack N7

- Réaliser un projet d’application significatif en groupe
- Mettre en pratique tous les enseignements de la formation
- Travailler à plusieurs sur un projet au travers d’outils de partage
- Programmer une application avec une partie back-end, une partie front-end, exécutable sur le web et sur mobile.
- 
### Compétences visées
- Suivre un projet de son idée jusqu’à son développement applicatif
- Suivre un cahier des charges
- Définir des tâches pour chaque membre du projet
- Suivre un calendrier strict
- Mettre en pratique une gestion de projet
- 
### Volume horaire
- 57 heures
- 
### Référent
- Patrick Rahmoune

### État des lieux

- Au 1/03/19, nous avons réalisé : la partie connexion, la page d'accueil, et la page "produit" sans le filtre.