from django.db import models
from datetime import date
from multiselectfield import MultiSelectField

# def get_image_path(filename):
    # Cette fonction sera à placer dans un autre fichier : elle permettra de récupérer
    # les photo qui seront dans le fichier static
    # return os.path.join('../../static/image', filename)
# Create your models here.

# Create your models here.
class User(models.Model):
    user_firstname = models.CharField("prénom", max_length=200, )
    user_lastname = models.CharField("nom", max_length=200, )
    user_email = models.EmailField("email", max_length=70, unique=True,)
    user_password = models.CharField("password", max_length=50,)
    user_birthdate = models.DateField("date d'anniversaire")
    user_created_on = models.DateField("date de création compte", default=date.today,)
    user_last_login = models.DateField('login', default=date.today)
    user_is_active = models.BooleanField("is active", default=False)
    user_newsletter = models.BooleanField("ok for newsletter", default=False)
    user_cguv = models.BooleanField("ok for cguv", default=False)    
    # user_profile_picture = models.ImageField("image de profil du user", upload_to=get_image_path, blank=True, null=True,)

    def __str__(self):
        return self.user_firstname + ' ' + self.user_lastname


class Artistan(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,)
    artistan_address = models.CharField("adresse", max_length=1024,)
    artistan_zipcode = models.CharField("code Postal", max_length=12,)
    artistan_city = models.CharField("ville", max_length=50,)
    artistan_siren = models.CharField("siren", max_length=20,)
    artistan_siret = models.CharField("siret", max_length=17,)
    artistan_ape = models.CharField("code APE", max_length=12,)
    # artistan_profile_picture = ImageField("image profil artisan", upload_to=get_image_path, blank=True, null=True,)

    def __str__(self):
            return self.user.user_firstname + ' ' + self.user.user_lastname

class Atelier(models.Model):
    ENVIRONMENT = (
        ('ext', 'En plein air'),
        ('int', "À l'atelier"),
    )
    PUBLIC = (
        ('adlt', "Adultes"),
        ('enf', "Jeune public"),
        ('hand', "Public empêchés"),
    )
    LEVEL = (
        ('D', 'Débutant'),
        ('I', 'Intermédiaire'),
        ('C', 'Confirmé'),
        ('T', 'Tous niveaux'),
    )
    LANGUAGE = (
        ('fr', 'Français'),
        ('eng', 'Anglais'),
        ('ger', 'Allemand'),
        ('esp', 'Espagnol'),
        ('port', 'Portugais'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    atelier_address = models.CharField("adresse", max_length=500,)
    atelier_zipcode = models.CharField("code Postal", max_length=12,)
    atelier_city = models.CharField("ville", max_length=128,)
    atelier_environment = MultiSelectField("environnement", max_length=15, max_choices=2, choices=ENVIRONMENT)
    atelier_public = MultiSelectField("type de public", max_length=15, max_choices=2, choices=PUBLIC)
    atelier_price = models.DecimalField("prix", max_digits=7, decimal_places=2,)
    atelier_nb_days = models.IntegerField("nbre / jours", )
    atelier_nb_hours = models.IntegerField("nbre / heures",)
    atelier_level = MultiSelectField("niveau", max_length=15, max_choices=4, choices=LEVEL)
    atelier_language = MultiSelectField("langue", max_length=6, max_choices=6,choices=LANGUAGE)
    atelier_title = models.CharField("titre", max_length=500,)
    atelier_description = models.TextField("description", max_length=512, blank=True, null=True)
    atelier_informations_pratiques = models.TextField("informations pratiques", max_length=1024, blank=True, null=True)
    atelier_is_active = models.BooleanField("atelier actif")
    atelier_is_validated = models.BooleanField("atelier valide")
    # atelier_photo1 = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    # atelier_photo2 = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    # atelier_photo3 = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    # atelier_photo4 = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    # atelier_video1

    def __str__(self):
        return self.atelier_title


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, )
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE)
    comment_content = models.CharField(max_length=200)
    comment_datetime = models.DateField(default=date.today)
    comment_is_validated = models.BooleanField()
    
    def __str__(self):
        return self.comment_content

class Session(models.Model):
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE)
    session_nb_participants = models.IntegerField('nombre de participants dans la session')

    def __str__(self):
        return 'Session ' + str(self.id)  + ' ' + self.atelier.atelier_title

class Category(models.Model):
    category_name = models.CharField('nom de la categorie', max_length=200,)

    def __str__(self):
        return self.category_name

class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    sub_category_name = models.CharField('nom de la categorie', max_length=200,)

    def __str__(self):
        return self.sub_category_name 


class AtelierSubCategory (models.Model):
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE)

    def __str__(self):
        return  self.sub_category.sub_category_name + ' ' + self.atelier.atelier_title


class SessionElement(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    element_date = models.DateField("Date element", default=date.today)
    element_start_hour = models.TimeField('heure de début de la session')
    element_end_hour = models.TimeField('heure de fin de la session')

    def __str__(self):
        return self.session.atelier.atelier_title + ' ' + self.element_date

class Reservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    reservation_status = models.CharField('status de la reservation', max_length=200, blank=True, null=True)
    reservation_date = models.DateField("Date de la reservation", default=date.today)


    def __str__(self):
        return self.user.user_lastname + ' ' + self.session.atelier.atelier_title

