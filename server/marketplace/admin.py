from django.contrib import admin
from django.contrib.admin import *
from marketplace.models import *

class InlineArtistanAdmin(admin.TabularInline):
    model = Artistan

class UserAdmin(admin.ModelAdmin):
   inlines = [InlineArtistanAdmin]

# Register your models here.
admin.site.register(User, UserAdmin)
admin.site.register(Artistan)
admin.site.register(Atelier)
admin.site.register(Comment)
admin.site.register(Session)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(AtelierSubCategory)
admin.site.register(SessionElement)
admin.site.register(Reservation)