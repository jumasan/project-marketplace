# -*- coding: utf-8 -*-
from invoke import task
import os
import sys
import django
sys.path.append('./tasks')


@task
def populateDB(ctx):
    '''
    Insère un jeu de données factices dans la base de données
    '''
    import createFakeData
    createFakeData.createFakeData()


@task
def emptyDB(ctx):
    '''
    Vide la base de données
    '''
    import emptyDatabase
    emptyDatabase.emptyDB()
