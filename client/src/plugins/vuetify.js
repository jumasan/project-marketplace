import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import 'vuetify/src/theme/default.styl'
// import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader

import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'md' || 'fa',
  theme: {
    primary: '#A73D54',
    secondary: '#EF3549',
    accent: '#99222F',
    error: '#DD1C1A',
    info: '#FFCB08',
    success: '#42F285',
    warning: '#FB5012',
    // custom colors we can use in each component directly in tags<>
    blacky: '#393E41',
    reddy: colors.red.accent1
  },
  options: {
    customProperties: true
  }
})
