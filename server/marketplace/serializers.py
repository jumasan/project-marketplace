from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework import serializers
from django.core.serializers import serialize
from marketplace.models import *


class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = "__all__"

    isArtistan = serializers.SerializerMethodField('_artistan')

    def _artistan(self, obj):
        return bool(Artistan.objects.filter(user=obj).first())


class ArtistanSerializer(ModelSerializer):
    class Meta:
        model = Artistan
        fields = "__all__"


class AtelierSerializer(ModelSerializer):
    class Meta:
        model = Atelier
        fields = "__all__"


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class SessionSerializer(ModelSerializer):
    class Meta:
        model = Session
        fields = "__all__"


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class SubCategorySerializer(ModelSerializer):
    class Meta:
        model = SubCategory
        fields = "__all__"


class AtelierSubCategorySerializer(ModelSerializer):
    class Meta:
        model = AtelierSubCategory
        fields = "__all__"


class SessionElementSerializer(ModelSerializer):
    class Meta:
        model = SessionElement
        fields = "__all__"


class ReservationSerializer(ModelSerializer):
    class Meta:
        model = Reservation
        fields = "__all__"
